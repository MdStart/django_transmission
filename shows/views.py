from django.shortcuts import render

from shows.logic import *
from shows.forms import AddSeriesForm


def add_series(request):
    if request.method == 'POST':
        form = AddSeriesForm(request.POST)
        if form.is_valid():
            series_name = form.cleaned_data['series_name']
            candidates = find_imdb_candidates(series_name)
            if candidates:
                context = {'candidates': candidates}
                return render(
                    request,
                    'shows/select-series-to-add.html',
                    context,
                )

    context = {'form': AddSeriesForm()}
    return render(request, 'shows/add-series.html', context)


def respond_to_selected_series(request):
    series_info = request.GET['series'].strip().split(',')
    series_name, result = add_selected_series(*series_info)
    context = {'series': series_name, 'result': result}
    return render(request, 'shows/add-series-result.html', context)
