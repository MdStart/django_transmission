from django.contrib import admin

from shows import models
from shows.tasks import populate_episodes_for_series


@admin.register(models.Series)
class SeriesAdmin(admin.ModelAdmin):
    search_fields = 'name',
    actions = ['force_populate_episodes_for_series']

    def force_populate_episodes_for_series(self, request, queryset):
        for series in queryset:
            populate_episodes_for_series(
                series_id=series.id,
                tvdb_id=series.tvdb_id,
            )

    force_populate_episodes_for_series.short_description = (
        "Repopulate episodes"
    )


@admin.register(models.Episode)
class EpisodeAdmin(admin.ModelAdmin):
    search_fields = 'series__name',
    raw_id_fields = 'series',
    list_display = 'series', 'season', 'episode', 'air_date',
