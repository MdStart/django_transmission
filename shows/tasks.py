import datetime

from django.db.models import Max

from django_transmission.celery import app
from django_transmission.utils import get_tvdb_client
from core.utils import str_to_date
from shows.models import Episode, Series


@app.task(queue='celery')
def _set_tvdb_id_for_series(series_id):
    """
    Set the tvdb id for a series.

    Args:
        series_id (int): the id of the series that will be updated
    """
    tvdb_client = get_tvdb_client()
    series = Series.objects.get(id=series_id)
    series.tvdb_id = tvdb_client.get_tvdb_id(series.imdb_id)
    series.save()


@app.task(queue='celery')
def populate_episodes_for_series(series_id, tvdb_id, max_air_date=None):
    """
    Create all episodes for a series.

    Args:
        series_id (int): the id of the series in the database
        tvdb_id (str): the tvdb id of the series
        max_air_date (str): the latest date that episodes exists in the db
    """
    tvdb_client = get_tvdb_client()
    all_episodes = tvdb_client.get_episodes(tvdb_id)
    future_episodes = [
        episode_info for episode_info in all_episodes
        if episode_info['firstAired'] and
        episode_info['airedSeason'] and
        episode_info['airedEpisodeNumber'] and
        (
            max_air_date is None or
            str_to_date(episode_info['firstAired']) >= max_air_date
        )
    ]
    for episode_info in future_episodes:
        season = episode_info['airedSeason']
        episode_number = episode_info['airedEpisodeNumber']
        air_date = episode_info['firstAired']

        episode, created = Episode.objects.get_or_create(
            series_id=series_id, episode=episode_number, season=season,
            defaults={'air_date': air_date},
        )
        if not created and episode.air_date != air_date:
            episode.air_date = air_date
            episode.save()


@app.task(queue='celery')
def set_tvdb_id_for_all_series():
    """Add all missing tvdb ids."""
    series = Series.objects.filter(tvdb_id__isnull=True).values_list(
        'id', flat=True,
    )
    for series_id in series:
        _set_tvdb_id_for_series.delay(series_id)


@app.task(queue='celery')
def populate_episodes():
    """Add all episodes for series without episodes."""
    series = Series.objects.filter(
        episode__isnull=True,
        tvdb_id__isnull=False,
    ).values('id', 'tvdb_id')
    for series_info in series:
        populate_episodes_for_series.delay(
            series_info['id'],
            series_info['tvdb_id'],
        )


@app.task(queue='celery')
def populate_new_episodes():
    """
    Add new episodes for all series that have episodes.

    If the database has episodes in the future, the series is skipped.
    """
    today = datetime.date.today()
    outdated_series = Episode.objects.values('series_id').annotate(
        Max('air_date')
    ).filter(air_date__max__lt=today).values(
        'series_id', 'series__tvdb_id', 'air_date__max'
    )
    for series_info in outdated_series:
        populate_episodes_for_series.delay(
            series_info['series_id'],
            series_info['series__tvdb_id'],
            series_info['air_date__max'],
        )
