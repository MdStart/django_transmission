from django import forms


class AddSeriesForm(forms.Form):
    series_name = forms.CharField(label='Series name', max_length=255)
