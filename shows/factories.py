import factory
from factory import fuzzy

from shows import models
from django.conf import settings


class SeriesFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Series

    name = factory.Sequence(lambda n: 'Series#{}'.format(n))
    imdb_id = factory.Sequence(lambda n: 'IMDB#{}'.format(n))
    eztv_id = factory.Sequence(lambda n: n)


class EpisodeFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Episode

    series = factory.SubFactory(SeriesFactory)
    season = fuzzy.FuzzyInteger(1, 15)
    episode = fuzzy.FuzzyInteger(1, 15)
    air_date = fuzzy.FuzzyDate(settings.MIN_DATE)
