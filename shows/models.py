import datetime

from django.db import models


class Series(models.Model):
    name = models.CharField(max_length=255, unique=True)
    download_after = models.DateField(default=datetime.date.today)
    imdb_id = models.CharField(max_length=255, unique=True)
    tvdb_id = models.CharField(
        max_length=255,
        unique=True,
        null=True,
        blank=True,
    )
    eztv_id = models.PositiveIntegerField(
        unique=True,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'series'

    def save(self, *args, **kwargs):
        self.tvdb_id = self.tvdb_id or None
        self.eztv_id = self.eztv_id or None
        super(Series, self).save(*args, **kwargs)


class Episode(models.Model):
    series = models.ForeignKey(Series, on_delete=models.CASCADE)
    season = models.PositiveIntegerField()
    episode = models.PositiveIntegerField()
    air_date = models.DateField()

    def __str__(self):
        return '{series} S{season:02d}E{episode:02d}'.format(
            series=self.series.name,
            season=self.season,
            episode=self.episode,
        )

    class Meta:
        unique_together = 'series', 'season', 'episode'
        get_latest_by = 'air_date'
