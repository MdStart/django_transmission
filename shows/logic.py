from django.conf import settings
from django.db.utils import IntegrityError

from django_transmission.utils import get_tvdb_client
from shows.models import Series


def find_imdb_candidates(series_name):
    """
    Args:
        series_name(str):
    Returns:
        list[dict[str: str]]
    """
    imdb_candidates = []
    tvdb_client = get_tvdb_client()
    try:
        information = tvdb_client.find_series_by_name(series_name)
    except LookupError:
        return imdb_candidates
    for candidate in information:
        tvdb_id = candidate['tvdb_id']
        imdb_id = tvdb_client.get_imdb_id(tvdb_id)
        if not imdb_id:
            continue
        imdb_candidates.append({
            'name': candidate['name'],
            'imdb_id': imdb_id,
            'year': candidate['air_date'].strip().split('-')[0],
            'img': settings.TVDB_IMAGE_URL.format(tvdb_id=tvdb_id),
            'link': settings.IMDB_SERIES_URL.format(imdb_id=imdb_id),
            'tvdb_id': tvdb_id,
        })
    return imdb_candidates


def add_selected_series(series_name, imdb_id, tvdb_id):
    """
    Args:
        series_name(str):
        imdb_id(str):
        tvdb_id(str):

    Returns:
        tuple[str, bool]
    """
    series_to_add = Series(
        name=series_name,
        download_after=settings.MIN_DATE,
        imdb_id=imdb_id,
        tvdb_id=tvdb_id,
    )
    try:
        series_to_add.save()
        result = True
    except ValueError or AttributeError:
        result = False
    except IntegrityError:
        result = True
    return series_name, result
