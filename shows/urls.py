from django.conf.urls import url

from shows import views

urlpatterns = [
    url(r'^add-series/', views.add_series, name='add_series'),
    url(
        r'^respond-to-selected-series/',
        views.respond_to_selected_series,
        name='respond_to_selected_series',
    )
]
