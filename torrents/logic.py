from django_transmission.utils import get_transmission_client
from torrents.models import Torrent


def active_torrents_info():
    """Find the name, ratio, and progress of active torrents."""
    transmission_client = get_transmission_client()
    torrents = {
        torrent.hashString: torrent
        for torrent in transmission_client.get_torrents()
    }
    active_torrents = Torrent.objects.filter(
        torrent_hash__in=torrents
    ).select_related('episode', 'episode__series')
    torrents_info = {
        str(torrent.episode): {
            'ratio': torrents[torrent.torrent_hash].ratio,
            'progress': torrents[torrent.torrent_hash].progress,
        } for torrent in active_torrents
    }
    return torrents_info
