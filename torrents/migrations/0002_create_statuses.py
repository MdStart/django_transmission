# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-06 13:31
from __future__ import unicode_literals

from django.db import migrations

STATUSES = (
    'aired',
    'acquired',
    'downloading',
    'downloaded',
    'removed',
)


def create_statuses(apps, _):
    Status = apps.get_model("torrents", "Status")
    for status in STATUSES:
        current_status = Status.objects.create()
        current_status.name = status.title()
        current_status.label = status
        current_status.save()


def revert_create_statuses(apps, _):
    Status = apps.get_model("torrents", "Status")
    for status in Status.objects.filter(label__in=STATUSES):
        status.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('torrents', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_statuses, revert_create_statuses),
    ]
