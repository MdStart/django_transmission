import datetime
from mock import MagicMock, patch

from django.test import TestCase
from django.conf import settings

from django_transmission.mocks import (
    mock_eztv_client,
    mock_transmission_client,
)
from shows.factories import EpisodeFactory, SeriesFactory
from torrents import tasks
from torrents.models import Torrent, Status
from torrents.factories import TorrentFactory


class TestCreateTorrentsToAcquire(TestCase):
    def setUp(self):
        self.status_acquired = Status.objects.get(label='acquired')
        self.yesterday = datetime.date.today() - datetime.timedelta(1)

    def test_aired_created(self):
        series = SeriesFactory.create(download_after=settings.MIN_DATE)
        EpisodeFactory.create(air_date=self.yesterday, series=series)
        tasks.create_torrents_to_acquire()
        self.assertEqual(
            Torrent.objects.filter(status__label='aired').count(), 1
        )

    def test_do_not_add_torrent_for_old_episode(self):
        EpisodeFactory.create(
            air_date=datetime.date.today(),
        )

        tasks.create_torrents_to_acquire()
        self.assertEqual(
            Torrent.objects.filter(status__label='aired').count(), 0
        )

    def test_do_not_add_torrent_for_unwanted_episode(self):
        series_download_after_today = SeriesFactory.create(
            download_after=datetime.date.today()
        )

        EpisodeFactory.create(
            air_date=self.yesterday,
            series=series_download_after_today
        )
        tasks.create_torrents_to_acquire()
        self.assertEqual(
            Torrent.objects.filter(status__label='aired').count(), 0
        )

    def test_do_not_add_existing_torrent(self):
        series_download_after_min_date = SeriesFactory.create(
            download_after=self.yesterday
        )

        episode_with_torrent = EpisodeFactory.create(
            air_date=self.yesterday,
            series=series_download_after_min_date
        )

        TorrentFactory.create(
            episode=episode_with_torrent,
            status=self.status_acquired
        )

        tasks.create_torrents_to_acquire()
        self.assertEqual(
            Torrent.objects.filter(status__label='aired').count(), 0
        )


class TestAcquireTorrents(TestCase):
    @patch(
        'torrents.tasks.get_eztv_client',
        new=MagicMock(side_effect=mock_eztv_client),
    )
    def test_create_acquired_torrent(self):
        status_aired = Status.objects.get(label='aired')
        TorrentFactory.create(status=status_aired)

        tasks.acquire_torrents()
        self.assertEqual(
            Torrent.objects.filter(status__label='acquired').count(), 1
        )


class TestAddAcquiredTorrents(TestCase):
    @patch(
        'torrents.tasks.get_transmission_client',
        new=MagicMock(side_effect=mock_transmission_client),
    )
    def test_no_acquired_after_add(self):
        status_acquired = Status.objects.get(label='acquired')
        TorrentFactory.create(status=status_acquired)
        TorrentFactory.create(status=status_acquired)
        tasks.add_acquired_torrents()
        self.assertEqual(
            Torrent.objects.filter(status__label='acquired').count(), 0
        )


class TestMarkDownloadedTorrents(TestCase):
    @patch(
        'torrents.tasks.get_transmission_client',
        new=MagicMock(side_effect=mock_transmission_client),
    )
    @patch(
        'torrents.tasks.notify_downloaded_torrents',
        new=MagicMock(),
    )
    def test_mark_finished_torrents(self):
        status_downloading = Status.objects.get(label='downloading')
        TorrentFactory.create(status=status_downloading)
        tasks.mark_downloaded_torrents()
        self.assertEqual(
            Torrent.objects.filter(status__label='downloaded').count(), 1
        )


class TestRemoveDownloadedTorrents(TestCase):
    @patch(
        'torrents.tasks.get_transmission_client',
        new=MagicMock(side_effect=mock_transmission_client),
    )
    def test_downloaded_torrent_change_status_to_removed(self):
        status_downloaded = Status.objects.get(label='downloaded')
        TorrentFactory.create(status=status_downloaded)
        tasks.remove_downloaded_torrents()
        self.assertEqual(
            Torrent.objects.filter(status__label='removed').count(), 1
        )
