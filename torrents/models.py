from django.db import models

from shows.models import Episode


class Status(models.Model):
    name = models.CharField(max_length=255, unique=True)
    label = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'statuses'


class Torrent(models.Model):
    episode = models.OneToOneField(Episode, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    torrent_hash = models.CharField(
        max_length=255, unique=True, null=True, blank=True,
    )
    magnet_link = models.CharField(
        max_length=511, unique=True, null=True, blank=True,
    )

    def __str__(self):
        return 'Torrent of {episode}'.format(episode=self.episode)

    def save(self, *args, **kwargs):
        self.torrent_hash = self.torrent_hash or None
        self.magnet_link = self.magnet_link or None
        super(Torrent, self).save(*args, **kwargs)
