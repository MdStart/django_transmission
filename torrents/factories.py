import factory
from factory import fuzzy

from shows.factories import EpisodeFactory
from torrents import models


class TorrentFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Torrent

    status = factory.Iterator(models.Status.objects.all())
    episode = factory.SubFactory(EpisodeFactory)
