from django.conf.urls import url

from torrents import views

urlpatterns = [
    url(r'^active/', views.active, name='active'),
]
