import json
from django.http import HttpResponse
from django.shortcuts import render

from torrents.logic import active_torrents_info


def active(request):
    if request.is_ajax():
        content = {'torrents': active_torrents_info()}
        return HttpResponse(
            json.dumps(content),
            content_type='application/json',
        )
    return render(request, 'torrents/active.html')
