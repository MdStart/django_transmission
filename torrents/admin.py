from django.contrib import admin

from torrents import models


@admin.register(models.Torrent)
class TorrentAdmin(admin.ModelAdmin):
    search_fields = 'episode__series__name',
    raw_id_fields = 'episode',
    list_filter = 'status',
    list_display = 'episode', 'status'
