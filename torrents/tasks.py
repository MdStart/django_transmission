import datetime

from django.db.models import F
from django.core.mail import send_mail

from django_transmission.celery import app
from django_transmission.utils import get_eztv_client, get_transmission_client
from torrents.models import Torrent, Status
from shows.models import Episode

from django.conf import settings


@app.task(queue='celery')
def notify_downloaded_torrents(downloaded_torrents):
    """
    Send email notification for downloaded torrents.

    Args:
        downloaded_torrents (list[str]): the recently downloaded torrents
    """
    message = settings.TORRENT_DOWNLOADED_MESSAGE.format(
        downloaded_torrents='\n'.join(downloaded_torrents)
    )
    send_mail(
        settings.TORRENT_DOWNLOADED_SUBJECT,
        message,
        settings.EMAIL_FROM_ADDRESS,
        settings.DEFAULT_RECIPIENTS,
        fail_silently=False,
    )


@app.task(queue='celery')
def create_torrents_to_acquire():
    """
    Create new torrents for the episodes that aired yesterday.

    Run once per day.
    """
    today = datetime.date.today()
    episodes_to_acquire = Episode.objects.filter(
        air_date__lt=today,
        air_date__gt=F('series__download_after'),
        torrent__isnull=True,
    )
    status = Status.objects.get(label='aired')
    for episode in episodes_to_acquire:
        Torrent.objects.create(episode=episode, status=status)


@app.task(queue='celery')
def acquire_torrents():
    """
    Acquire the magnet links for the episodes that have aired already.
    """
    torrents_to_acquire = Torrent.objects.filter(
        status__label='aired',
    ).select_related('episode', 'episode__series')
    status = Status.objects.get(label='acquired')
    eztv_client = get_eztv_client()

    for torrent in torrents_to_acquire:
        episode = torrent.episode
        series = episode.series
        if series.eztv_id is not None:
            search_method = eztv_client.search_by_id_and_episode
            search_term = series.eztv_id
        else:
            search_method = eztv_client.search_by_series_and_episode
            search_term = str(series).replace("'", '')

        try:
            torrents_found = search_method(
                search_term,
                episode.season,
                episode.episode,
            )
        except LookupError:
            continue
        if torrents_found:
            torrent.status = status
            # torrents_found are already sorted by seeders,
            # therefore it makes sense to pick the first one.
            torrent.magnet_link = torrents_found[0]['magnet_link']
            torrent.save()


@app.task(queue='celery')
def add_acquired_torrents():
    """
    Add all magnet links to deluge.
    """
    acquired_torrents = Torrent.objects.filter(status__label='acquired')
    status = Status.objects.get(label='downloading')
    transmission_client = get_transmission_client()

    for torrent in acquired_torrents:
        response = transmission_client.add_uri(torrent.magnet_link)
        torrent.torrent_hash = list(response.values())[0].hashString
        torrent.status = status
        torrent.save()


@app.task(queue='celery')
def mark_downloaded_torrents():
    """
    Change the status in the db of the torrents that have finished downloading.
    """
    downloading_torrents = Torrent.objects.filter(status__label='downloading')
    status = Status.objects.get(label='downloaded')
    transmission_client = get_transmission_client()
    downloaded_torrents = []

    for torrent in downloading_torrents:
        info = transmission_client.get_torrent(torrent.torrent_hash)
        if info.progress == 100:
            torrent.status = status
            torrent.save()
            downloaded_torrents.append(str(torrent))

    if downloaded_torrents:
        notify_downloaded_torrents.delay(downloaded_torrents)


@app.task(queue='celery')
def remove_downloaded_torrents():
    """
    Remove torrents from deluge that have reached the limit ratio.
    """
    downloaded_torrents = Torrent.objects.filter(status__label='downloaded')
    status = Status.objects.get(label='removed')
    transmission_client = get_transmission_client()

    for torrent in downloaded_torrents:
        info = transmission_client.get_torrent(torrent.torrent_hash)
        if info.ratio >= settings.RATIO_LIMIT:
            torrent.status = status
            torrent.save()
            transmission_client.remove_torrent(torrent.torrent_hash)
