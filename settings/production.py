from settings.common import *

DEBUG = False
X_FRAME_OPTIONS = 'DENY'
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

STATICFILES_DIRS = []
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
