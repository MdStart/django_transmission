from datetime import timedelta

from celery.schedules import crontab
from kombu.serialization import register

from core.serialization import trans_dumps, trans_loads

register(
    'trans_json', trans_dumps, trans_loads,
    content_type='application/x-trans-json',
    content_encoding='utf-8'
)

BROKER_BASE = 'amqp://{user}:{password}@{host}:{port}/{vhost}'
CELERY_ACCEPT_CONTENT = ['trans_json']
CELERY_TASK_SERIALIZER = 'trans_json'
CELERY_RESULT_SERIALIZER = 'trans_json'
CELERY_ACKS_LATE = True
CELERY_IGNORE_RESULT = True
CELERY_RESULT_BACKEND = 'rpc://'
CELERY = {
    'HOST': '127.0.0.1',
    'PORT': 5672,
    'VHOST': 'transmission_vhost',
}

CELERYBEAT_SCHEDULE = {
    'set_tvdb_id_for_all_series': {
        'task': 'shows.tasks.set_tvdb_id_for_all_series',
        'schedule': crontab(minute=0, hour=0),
    },
    'populate_episodes': {
        'task': 'shows.tasks.populate_episodes',
        'schedule': crontab(minute=5, hour=0),
    },
    'populate_new_episodes': {
        'task': 'shows.tasks.populate_new_episodes',
        'schedule': crontab(minute=10, hour=0),
    },
    'create_torrents_to_acquire': {
        'task': 'torrents.tasks.create_torrents_to_acquire',
        'schedule': crontab(minute=15, hour=0),
    },
    'acquire_torrents': {
        'task': 'torrents.tasks.acquire_torrents',
        'schedule': timedelta(minutes=15),
    },
    'add_acquired_torrents': {
        'task': 'torrents.tasks.add_acquired_torrents',
        'schedule': timedelta(minutes=15),
    },
    'mark_downloaded_torrents': {
        'task': 'torrents.tasks.mark_downloaded_torrents',
        'schedule': timedelta(minutes=15),
    },
    'remove_downloaded_torrents': {
        'task': 'torrents.tasks.remove_downloaded_torrents',
        'schedule': timedelta(minutes=15),
    },
}
