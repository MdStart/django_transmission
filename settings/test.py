from settings.common import *

CELERY_ALWAYS_EAGER = True

DATABASES['default']['ENGINE'] = 'django.db.backends.sqlite3'
DATABASES['default']['NAME'] = 'test_data.db'
