import datetime

TVDB_IMAGE_URL = 'http://thetvdb.com/banners/posters/{tvdb_id}-1.jpg'
IMDB_SERIES_URL = 'http://www.imdb.com/title/{imdb_id}/'

EMAIL_FROM_ADDRESS = 'notifications@raspberry.pi'

MIN_DATE = datetime.date(2014, 1, 1)
RATIO_LIMIT = 1.5
