const timeout = 5000;

function refreshTorrents() {
    $.ajax({
        url: '/torrents/active/',
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            $('.torrent-table tbody').empty();
            $.each(data.torrents, function(key, value) {
                $('.torrent-table tbody').append(`
                    <tr><td>${key}</td>
                    <td class="numeric">${value.ratio.toFixed(3)}</td>
                    <td class="numeric">${value.progress.toFixed((2))}%</td></tr>
                `);
            });
        },
        complete: function () {
            setTimeout(refreshTorrents, timeout);
        }
    });
}

$(document).ready(function() {
    $('.torrent-table').DataTable({
        "paging": false,
        "order": [[2, "asc"], [1, "desc"]]
    });
} );

$(document).ready(function() {
    refreshTorrents();
});
