Prerequisites
---

The following guide assumes that the following are already installed in your system:

* rabbitmq
* transmission-cli
* postgres

and the respective services are started and enabled

1) clone the project and cd into it
```
git clone git@gitlab.com:stema/django_transmission.git
cd django_transmission
```

2) install the requirements

```
virtualenv transmission
. transmission/bin/activate
pip install -r requirements.txt
```

3) create the cache folder
```
mkdir django_cache
```

3) copy and populate the local settings
```
cp settings/local.py.template settings/local.py
```

Development
---

For convenience, two shortcuts can be taken

1) if there is no need for rabbitmq and celery
you can add to ```settings/local.py``` the following line:
```
CELERY_ALWAYS_EAGER = True
```

2) you can use sqlite, instead of postgres
add to ```settings/local.py``` the following line:
```
DATABASES['default']['ENGINE'] = 'django.db.backends.sqlite3'
DATABASES['default']['NAME'] = 'dev_database.db'
```
