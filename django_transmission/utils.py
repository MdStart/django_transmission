from eztv_api import EztvClient
from django.core.cache import cache
from tvdb_api_client import TVDBClient
from transmissionrpc import Client as TransmissionClient

from core.utils import get_country

from django.conf import settings


def get_tvdb_client():
    return TVDBClient(
        username=settings.TVDB_USER_NAME,
        userkey=settings.TVDB_USER_KEY,
        apikey=settings.TVDB_API_KEY,
        cache=cache,
    )


def get_eztv_client():
    country = get_country()
    if country not in settings.WHITELISTED_COUNTRIES:
        raise RuntimeError('{} is not whitelisted.'.format(country))
    return EztvClient(
        url=settings.TORRENT_SITE_URL,
    )


def get_transmission_client():
    return TransmissionClient(
        address=settings.TRANSMISSION_ADDRESS,
        port=settings.TRANSMISSION_PORT,
    )
