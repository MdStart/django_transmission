import os
import sys

from celery import Celery

settings_file = "test" if "test" in sys.argv else "local"

os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE",
    "settings.{}".format(settings_file),
)


from django.conf import settings  # noqa

app = Celery('django_transmission')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
