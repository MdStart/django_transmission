from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^torrents/', include('torrents.urls', namespace='torrents')),
    url(r'^shows/', include('shows.urls', namespace='shows')),
]
