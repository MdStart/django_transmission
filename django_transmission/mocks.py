from factory.fuzzy import FuzzyText
from mock import MagicMock

from django.conf import settings


class MockTorrent:
    def __init__(self):
        self.ratio = settings.RATIO_LIMIT
        self.progress = 100.0
        self.hashString = FuzzyText().fuzz()


def mock_eztv_client():
    client = MagicMock()
    client.search_by_id_and_episode = MagicMock(side_effect=[[{
        'name': 'series S01E12 HDTV',
        'season': 1,
        'episode': 12,
        'size': '743.68 MB',
        'seeders': 124,
        'magnet_link': ['magnet_link#1', 'magnet_link#2'],
        'torrent_links': ['http://example.com/series.torrent'],
        }]])
    client.search_by_series_and_episode = MagicMock(side_effect=[[{
        'name': 'series S01E12 HDTV',
        'season': 1,
        'episode': 12,
        'size': '743.68 MB',
        'seeders': 124,
        'magnet_link': ['magnet_link#1', 'magnet_link#2'],
        'torrent_links': ['http://example.com/series.torrent'],
    }]])
    return client


def mock_transmission_client():
    client = MagicMock()
    client.add_uri = MagicMock(
        side_effect=[
            {1: MockTorrent()},
            {2: MockTorrent()},
        ],
    )
    client.remove_torrent = MagicMock()
    client.get_torrent = MagicMock(
        return_value=MockTorrent()
    )
    return client
