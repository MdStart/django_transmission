import datetime
import json
import requests


def str_to_date(string, date_format='%Y-%m-%d'):
    return datetime.datetime.strptime(string, date_format).date()


def decode_items(dictionary):
    if not isinstance(dictionary, dict):
        return dictionary

    out = {}
    for key, value in dictionary.items():
        if isinstance(key, bytes):
            key = key.decode('utf-8')
        if isinstance(value, bytes):
            value = value.decode('utf-8')
        elif isinstance(value, dict):
            value = decode_items(value)
        elif isinstance(value, tuple):
            value = tuple(decode_items(item) for item in value)
        elif isinstance(value, list):
            value = [decode_items(item) for item in value]
        out[key] = value

    return out


def get_country():
    response = requests.get('https://duckduckgo.com/country.json')
    return json.loads(response.content)['country']
