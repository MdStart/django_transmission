import datetime
import json

import dateutil.parser


class TransEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return {
                '__type__': '__datetime__',
                'iso_format': obj.isoformat()
            }
        if isinstance(obj, datetime.date):
            return {
                '__type__': '__date__',
                'iso_format': obj.isoformat()
            }
        if isinstance(obj, datetime.time):
            return {
                '__type__': '__time__',
                'iso_format': obj.isoformat()
            }
        else:
            return json.JSONEncoder.default(self, obj)


def trans_decoder(obj):
    if '__type__' in obj:
        if obj['__type__'] == '__datetime__':
            return dateutil.parser.parse(obj['iso_format'])
        if obj['__type__'] == '__date__':
            return dateutil.parser.parse(obj['iso_format']).date()
        if obj['__type__'] == '__time__':
            return dateutil.parser.parse(obj['iso_format']).time()
    return obj


def trans_dumps(obj):
    return json.dumps(obj, cls=TransEncoder)


def trans_loads(obj):
    return json.loads(obj, object_hook=trans_decoder)
